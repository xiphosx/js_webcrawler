#JavaScript webcrawler

This is a simple web crawler in JavaScript that will get a web page, find a word on a given web page, and if it isn't found, collects all the links on that page so we can visit them.

The three libraries we'll be using is Request, Cheerio, and URL. NPM will be used to install these libraries.

The package.json describes the project and specifies the dependencies.

When ready type npm crawler.js